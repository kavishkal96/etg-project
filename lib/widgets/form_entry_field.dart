import 'package:flutter/material.dart';

Widget entryField(String title, String hintText, double width,
    {bool isPassword = false}) {
  return Container(
    width: width,
    margin: EdgeInsets.symmetric(vertical: 10),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          title,
          style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 13,
              color: Colors.grey[700]),
        ),
        SizedBox(
          height: 10,
        ),
        TextField(
            style: TextStyle(fontSize: 13),
            obscureText: isPassword,
            decoration: InputDecoration(
                contentPadding:
                    const EdgeInsets.symmetric(vertical: 2.0, horizontal: 10),
                hintText: hintText,
                // helperText: 'Keep it short, this is just a demo.',
                hintStyle: TextStyle(color: Color(0xFFBDBDD3)),
                border: OutlineInputBorder(
                    borderSide: BorderSide(width: 0.0, color: Colors.green),
                    borderRadius: BorderRadius.circular(5.0)),
                fillColor: Color(0xFFFAFAFA),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(4)),
                  borderSide: BorderSide(width: 1, color: Color(0xFFC4C4C4)),
                ),
                errorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(4)),
                    borderSide: BorderSide(width: 1, color: Colors.black)),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(4)),
                  borderSide: BorderSide(width: 1.5, color: Color(0xFF8429CE)),
                ),
                filled: true))
      ],
    ),
  );
}
