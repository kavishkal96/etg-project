import 'package:etg/screens/login_screen.dart';
import 'package:flutter/material.dart';

Widget commonAppBar(String title, BuildContext context) {
  return AppBar(
    backgroundColor: Colors.black,
    title: Text(title),
    actions: [
      CircleAvatar(
        backgroundColor: Colors.red,
        //child: Image.network("https://learnwithmalisha.com/"),
      ),
      Padding(
        padding: const EdgeInsets.all(20.0),
        child: IconButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => LoginScreen()),
              );
            },
            icon: Icon(
              Icons.logout,
              size: 25,
            )),
      )
    ],
    leading: new IconButton(
      icon: new Icon(Icons.arrow_back, color: Colors.white),
      onPressed: () => Navigator.of(context).pop(),
    ),
  );
}

// Widget appBar(String text, IconButton iconButton) {
//   return AppBar(
//     title: Text(
//       text,
//       //style: AppTheme.screenTitleStyle(),
//     ),
//     centerTitle: true,
//     leading: IconButton(icon: iconButton, onPressed: () {}),
//    // backgroundColor: AppTheme.mainThemeColor(),
//     brightness: Brightness.dark,
//   );
// }
