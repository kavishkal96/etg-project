import 'package:flutter/material.dart';

Widget submitButton(String title, context) {
  return Container(
    width: MediaQuery.of(context).size.width,
    // width: 100,
    padding: EdgeInsets.symmetric(vertical: 12),
    margin: EdgeInsets.only(bottom: 10),
    alignment: Alignment.center,
    decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(5)),
        boxShadow: <BoxShadow>[
          BoxShadow(
              color: Colors.black45,
              offset: Offset(2, 4),
              blurRadius: 5,
              spreadRadius: 2)
        ],
        gradient: LinearGradient(
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            colors: [Color(0xFF30D6AC), Color(0xFF3C81DB)])),
    child: Text(
      title,
      style: TextStyle(fontSize: 20, color: Colors.white),
    ),
  );
}
