import 'package:flutter/material.dart';

class ETGHomeCard extends StatelessWidget {
  late final Color color;
  late final IconData iconName;
  late final Function() onTap;
  late final String cardtitle;
  ETGHomeCard(
      {required this.color,
      required this.iconName,
      required this.onTap,
      required this.cardtitle});

  @override
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        clipBehavior: Clip.hardEdge,
        child: InkWell(
          splashColor: Colors.blue.withAlpha(50),
          onTap: onTap,
          child: SizedBox(
            height: 150,
            width: 150,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  iconName,
                  color: color,
                  size: 70.0,
                  semanticLabel: 'Text to announce in accessibility modes',
                ),
                Text(
                  cardtitle,
                  style: TextStyle(
                      fontSize: 14,
                      color: Color(0xFF2C2C2C),
                      fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
