import 'package:flutter/material.dart';

class MyProfileScreen extends StatelessWidget {
  const MyProfileScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("My Profile"),
        backgroundColor: Colors.black,
        actions: [
          CircleAvatar(
            backgroundColor: Colors.red,
            //child: Image.network("https://learnwithmalisha.com/"),
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: Icon(Icons.logout),
          )
        ],
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      backgroundColor: Colors.grey,
      body: Padding(
        padding: const EdgeInsets.all(38.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 35,
            ),
            Wrap(
              alignment: WrapAlignment.start,
              crossAxisAlignment: WrapCrossAlignment.start,
              children: [
                Text(
                  "Name:",
                  style: TextStyle(
                      fontSize: 18,
                      color: Color(0xFF030303),
                      fontWeight: FontWeight.bold),
                ),
                Text(
                  "Johne Smith",
                  style: TextStyle(
                      fontSize: 18,
                      color: Color(0xFF030303),
                      fontWeight: FontWeight.bold),
                ),
              ],
            ),
            Wrap(
              children: [
                Text(
                  "Email:",
                  style: TextStyle(
                      fontSize: 18,
                      color: Color(0xFF030303),
                      fontWeight: FontWeight.bold),
                ),
                Text(
                  "johnesmith@gmail.com",
                  style: TextStyle(
                      fontSize: 18,
                      color: Color(0xFF030303),
                      fontWeight: FontWeight.bold),
                ),
              ],
            ),
            Wrap(
              children: [
                Text(
                  "Username:",
                  style: TextStyle(
                      fontSize: 18,
                      color: Color(0xFF030303),
                      fontWeight: FontWeight.bold),
                ),
                Text(
                  "Johnes",
                  style: TextStyle(
                      fontSize: 18,
                      color: Color(0xFF030303),
                      fontWeight: FontWeight.bold),
                ),
              ],
            ),
            Wrap(
              children: [
                Text(
                  "Account Type:",
                  style: TextStyle(
                      fontSize: 18,
                      color: Color(0xFF030303),
                      fontWeight: FontWeight.bold),
                ),
                Text(
                  "User",
                  style: TextStyle(
                      fontSize: 18,
                      color: Color(0xFF030303),
                      fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
