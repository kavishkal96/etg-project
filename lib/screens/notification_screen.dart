import 'dart:ui';

import 'package:etg/widgets/common_appbar.dart';
import 'package:etg/widgets/notifiaction-card.dart';
import 'package:flutter/material.dart';

class NotificationScreen extends StatelessWidget {
  const NotificationScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: const Size.fromHeight(80),
          child: commonAppBar("Notifications", context)),
      backgroundColor: const Color(0xFF29282C),
      body: Padding(
          padding: const EdgeInsets.all(10.0),
          child: ListView(
            children: [
              InkWell(
                onTap: () async {
                  customDialog(context);
                },
                child: notificationCard("Promotion",
                    "Get 25% discount every item you buy, Only today"),
              ),
              InkWell(
                  onTap: () async {
                    customDialog(context);
                  },
                  child: notificationCard(
                      "Announcemet", "bookfair 2021 open today until 6pm"))
            ],
          )),
    );
  }

  Future<dynamic> customDialog(context) {
    return showDialog(
        context: context,
        // barrierColor: Colors.red,
        builder: (BuildContext context) {
          return BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 2, sigmaY: 2),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0)),
                child: Container(
                  height: 400.0,
                  width: 400.0,
                  decoration:
                      BoxDecoration(borderRadius: BorderRadius.circular(10.0)),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Text(
                          "Promotion",
                          style: TextStyle(
                              fontSize: 18,
                              color: Color(0xFF272727),
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Text(
                          "In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content. Lorem ipsum may be used as a placeholder before final copy is available.",
                          style: TextStyle(
                              fontSize: 16,
                              color: Color(0xFF312D2D),
                              fontWeight: FontWeight.bold),
                        ),
                      )
                    ],
                  ),
                ),
              ));
        });
  }
}
