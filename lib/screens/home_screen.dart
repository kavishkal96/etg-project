
import 'package:etg/screens/complaints_screen.dart';
import 'package:etg/screens/db_item_list.dart';
import 'package:etg/screens/firebaseintegrate/flogin_screen.dart';
import 'package:etg/screens/firebaseintegrate/item_list.dart';
import 'package:etg/screens/myprofile_screen.dart';
import 'package:etg/screens/notification_screen.dart';
import 'package:etg/screens/searchaproduct_screen.dart';
import 'package:etg/widgets/etg_home_card.dart';
import 'package:flutter/material.dart';

import 'login_screen.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        actions: [
          Row(
            children: [
              Container(
                width: 50,
                child: Wrap(
                  children: [
                    Text(
                      "John",
                      style: TextStyle(
                          fontSize: 12,
                          color: Color(0xFFEEEEEE),
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      "Smith",
                      style: TextStyle(
                          fontSize: 12,
                          color: Color(0xFFEEEEEE),
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
              CircleAvatar(
                child: Icon(Icons.person),
                backgroundColor: Colors.orange,
                //child: Image.network("https://learnwithmalisha.com/"),
              ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: IconButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => LoginScreen()),
                      );
                    },
                    icon: Icon(Icons.logout)),
              )
            ],
          ),
        ],
        leading: Row(children: []),
      ),
      backgroundColor: Colors.black,
      body: Container(
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          color: const Color(0xFF000000),
          image: new DecorationImage(
            colorFilter: new ColorFilter.mode(
                Colors.black.withOpacity(0.2), BlendMode.dstATop),
            image: new ExactAssetImage('assets/images/bg.jpg'),
            fit: BoxFit.cover,
          ),
        ),
        child: Center(
          child: GridView(
            shrinkWrap: false,
            gridDelegate:
                SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
            children: <Widget>[
              ETGHomeCard(
                color: Colors.black87,
                iconName: Icons.account_circle,
                onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MyProfileScreen()),
                ),
                cardtitle: "My Profile",
              ),
              ETGHomeCard(
                color: Colors.black87,
                iconName: Icons.notifications,
                onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => NotificationScreen()),
                ),
                cardtitle: "Notifications",
              ),
              ETGHomeCard(
                color: Colors.black87,
                iconName: Icons.comment,
                onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ComplainScreen()),
                ),
                cardtitle: "Complains",
              ),
              ETGHomeCard(
                color: Colors.black87,
                iconName: Icons.search_rounded,
                onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => SearchProductScreen()),
                ),
                cardtitle: "Find Products",
              ),
              ETGHomeCard(
                color: Colors.black87,
                iconName: Icons.map,
                onTap: () 
                => Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ItemList()),
                     // DbItemList()),
                ),
                cardtitle: "Directionss",
              ),

               ETGHomeCard(
                color: Colors.black87,
                iconName: Icons.add_business,
                onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => LoginScreenf()),
                ),
                cardtitle: "Add Stall ",
              ),
            ],
          ),
        ),
      ),
    );
  }
}
