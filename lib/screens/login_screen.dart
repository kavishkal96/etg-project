

import 'package:etg/screens/home_screen.dart';
import 'package:etg/screens/signup_screen.dart';
import 'package:etg/widgets/form_entry_field.dart';
import 'package:etg/widgets/submit_button.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //resizeToAvoidBottomInset: false,
      body: SingleChildScrollView(
        physics: ClampingScrollPhysics(),
        // physics: ClampingScrollPhysics(),
        child: Container(
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            color: const Color(0xFF000000),
            image: new DecorationImage(
              colorFilter: new ColorFilter.mode(
                  Colors.black.withOpacity(0.2), BlendMode.dstATop),
              image: new ExactAssetImage('assets/images/bg.jpg'),
              fit: BoxFit.cover,
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(38.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: 10,
                ),
                //  SizedBox(height: 60, child: Image.asset('')),
                Row(
                  children: [
                    Text(
                      "Log In",
                      style: TextStyle(
                          fontSize: 45,
                          color: Color(0xFF44A0B8),
                          fontWeight: FontWeight.bold),
                      textAlign: TextAlign.left,
                    ),
                    SizedBox(
                      width: 100,
                      height: 50,
                    )
                  ],
                ),
                Row(
                  children: [
                    Text(
                      "Please fill the following to Log In.",
                      style: TextStyle(
                          fontSize: 16,
                          color: Color(0xFFEEEEEE),
                          fontWeight: FontWeight.bold),
                      textAlign: TextAlign.left,
                    ),
                  ],
                ),
                SizedBox(
                  width: 30,
                  height: 50,
                ),
                entryField(
                    "Email id", "Enter your email address", double.infinity),
                entryField(
                  "Password",
                  "Enter your passcode",
                  double.infinity,
                  isPassword: true,
                ),
                SizedBox(
                  height: 10,
                ),
                InkWell(
                    onTap: () =>
                        Navigator.push(context, MaterialPageRoute(builder: (_) {
                          return HomeScreen();
                        })),
                    child: submitButton("Login", context)),
                SizedBox(
                  height: 8,
                ),
                Row(
                  children: [
                    SizedBox(
                      width: 198,
                    ),
                    Text(
                      "Need help?",
                      style: TextStyle(
                          fontSize: 16,
                          color: Color(0xFF858585),
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    Text(
                      "Don't have a account?",
                      style: TextStyle(
                          fontSize: 16,
                          color: Color(0xFF858585),
                          fontWeight: FontWeight.bold),
                    ),
                    InkWell(
                        onTap: () => Navigator.push(context,
                                MaterialPageRoute(builder: (_) {
                              return SignUpScreen();
                            })),
                        child: Padding(
                          padding: const EdgeInsets.only(left: 10),
                          child: Text(
                            "Sign Up",
                            style: TextStyle(
                                fontSize: 16,
                                color: Color(0xFFEEEEEE),
                                fontWeight: FontWeight.bold),
                          ),
                        )),
                  ],
                ),
        
              ],
            ),
          ),
        ),
      ),
    );
  }
}
