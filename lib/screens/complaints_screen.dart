import 'package:etg/screens/home_screen.dart';
import 'package:etg/screens/login_screen.dart';
import 'package:etg/widgets/common_appbar.dart';
import 'package:flutter/material.dart';
import 'package:etg/widgets/form_entry_field.dart';
import 'package:etg/widgets/submit_button.dart';

class ComplainScreen extends StatelessWidget {
  const ComplainScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(80),
        child: commonAppBar("Complains", context),
      ),
      body: Container(
        color: const Color(0xFF29282C),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 30),
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.center,
            children: [
              entryField(
                  "Type", "Enter type of the Complains", double.infinity),
              entryField("Contact Number", "Enter your contact number",
                  double.infinity),
              entryField(
                "Topic",
                "Enter topic of the complaint",
                double.infinity,
                isPassword: true,
              ),
              TextField(
                decoration: InputDecoration(
                    hintText: "Message",
                    hintStyle: TextStyle(fontSize: 12),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                      borderSide: BorderSide.none,
                    ),
                    fillColor: Colors.white,
                    filled: true),
                minLines:
                    6, // any number you need (It works as the rows for the textarea)
                keyboardType: TextInputType.multiline,
                maxLines: null,
              ),
              SizedBox(
                height: 35,
              ),
              InkWell(
                  onTap: () =>
                      Navigator.push(context, MaterialPageRoute(builder: (_) {
                        return HomeScreen();
                      })),
                  child: submitButton("Submit", context)),
              SizedBox(
                height: 8,
              ),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
