import 'package:etg/screens/home_screen.dart';
import 'package:etg/screens/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:etg/widgets/form_entry_field.dart';
import 'package:etg/widgets/submit_button.dart';

class SignUpScreen extends StatelessWidget {
  const SignUpScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //resizeToAvoidBottomInset: false,
      body: SingleChildScrollView(
        physics: ClampingScrollPhysics(),
        // physics: ClampingScrollPhysics(),
        child: Container(
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            color: const Color(0xFF000000),
            image: new DecorationImage(
              colorFilter: new ColorFilter.mode(
                  Colors.black.withOpacity(0.2), BlendMode.dstATop),
              image: new ExactAssetImage('assets/images/bg.jpg'),
              fit: BoxFit.cover,
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(38.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: 10,
                ),
                //  SizedBox(height: 60, child: Image.asset('')),
                Row(
                  children: [
                    Text(
                      "Sign Up",
                      style: TextStyle(
                          fontSize: 45,
                          color: Color(0xFF44A0B8),
                          fontWeight: FontWeight.bold),
                      textAlign: TextAlign.left,
                    ),
                    SizedBox(
                      width: 100,
                      height: 50,
                    )
                  ],
                ),
                Row(
                  children: [
                    Text(
                      "Please fill the following to register.",
                      style: TextStyle(
                          fontSize: 16,
                          color: Color(0xFFEEEEEE),
                          fontWeight: FontWeight.bold),
                      textAlign: TextAlign.left,
                    ),
                  ],
                ),
                SizedBox(
                  width: 30,
                  height: 50,
                ),
                entryField("User Name", "Enter your username", double.infinity),
                entryField("Email", "Enter your email", double.infinity),
                entryField(
                  "Password",
                  "Enter Password",
                  double.infinity,
                  isPassword: true,
                ),
                entryField(
                  "Confirm Password",
                  "Enter Confirm Password",
                  double.infinity,
                  isPassword: true,
                ),
                SizedBox(
                  height: 10,
                ),
                InkWell(
                    onTap: () =>
                        Navigator.push(context, MaterialPageRoute(builder: (_) {
                          return HomeScreen();
                        })),
                    child: submitButton("Login", context)),
                SizedBox(
                  height: 8,
                ),

                SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    Text(
                      "Already a member?",
                      style: TextStyle(
                          fontSize: 16,
                          color: Color(0xFF858585),
                          fontWeight: FontWeight.bold),
                    ),
                    InkWell(
                        onTap: () => Navigator.push(context,
                                MaterialPageRoute(builder: (_) {
                              return LoginScreen();
                            })),
                        child: Padding(
                          padding: const EdgeInsets.only(left: 10),
                          child: Text(
                            "Log In",
                            style: TextStyle(
                                fontSize: 16,
                                color: Color(0xFFEEEEEE),
                                fontWeight: FontWeight.bold),
                          ),
                        )),
                  ],
                ),
                // InkWell(
                //     onTap: () =>
                //         Navigator.push(context, MaterialPageRoute(builder: (_) {
                //           return HomeScreen();
                //         })),
                //     child: submitButton("Register", context)),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
