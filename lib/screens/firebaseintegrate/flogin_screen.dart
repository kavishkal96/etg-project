import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:etg/screens/firebaseintegrate/flogin_form.dart';
import 'package:firebase_core/firebase_core.dart';

import 'package:flutter/material.dart';

class LoginScreenf extends StatefulWidget {
  @override
  _LoginScreenfState createState() => _LoginScreenfState();
}

class _LoginScreenfState extends State<LoginScreenf> {
  final FocusNode _uidFocusNode = FocusNode();

  Future<FirebaseApp> _initializeFirebase() async {
    FirebaseApp firebaseApp =  await Firebase.initializeApp();

    return firebaseApp;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => _uidFocusNode.unfocus(),
    child:
       SafeArea(
          child:
             Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
        ),
        backgroundColor: Colors.black.withOpacity(0.5),
          
       body:    Padding(
            padding: const EdgeInsets.only(
              left: 16.0,
              right: 16.0,
              bottom: 20.0,
            ),
            child: Container(
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          color: const Color(0xFF888B6B),
          image: new DecorationImage(
            colorFilter: new ColorFilter.mode(
                Colors.black.withOpacity(0.2), BlendMode.dstATop),
            image: new ExactAssetImage('assets/images/bg.jpg'),
            fit: BoxFit.cover,
          ),
        ),
        child:
            
            
             Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                Row(),
                Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Flexible(
                        flex: 1,
                        child:Text("Login to",
                                style: TextStyle(
                                  color: Colors.amber,
                                  fontSize: 20,
                                ), )
                      ),
                      SizedBox(height: 20),
                      Text(
                        'Add Stall ',
                        style: TextStyle(
                          color: Colors.amber,
                          fontSize: 40,
                        ),
                      ),
                    
                    ],
                  ),
                ),
                FutureBuilder(
                  future: _initializeFirebase(),
                  builder: (context, snapshot) {
                    if (snapshot.hasError) {
                      return Text('Error initializing Firebase');
                    } else if (snapshot.connectionState ==
                        ConnectionState.done) {
                      return LoginForm(focusNode: _uidFocusNode);
                    }
                    return CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(
                        Colors.amber,
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    ));
  }
}
