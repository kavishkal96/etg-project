
import 'dart:io';

import 'package:etg/screens/firebaseintegrate/custom_colors.dart';
import 'package:etg/screens/firebaseintegrate/fcustom_form_field.dart';
import 'package:etg/screens/firebaseintegrate/validator.dart';
import 'package:etg/utils/database.dart';
import 'package:flutter/material.dart';


class AddItemForm extends StatefulWidget {
  final FocusNode titleFocusNode;
  final FocusNode descriptionFocusNode;

  const AddItemForm({
    required this.titleFocusNode,
    required this.descriptionFocusNode,
  });

  @override
  _AddItemFormState createState() => _AddItemFormState();
}

class _AddItemFormState extends State<AddItemForm> {
//image upload

//   firebase_storage.FirebaseStorage storage =
//       firebase_storage.FirebaseStorage.instanceFor(
//           bucket: 'secondary-storage-bucket');

// // or
//   firebase_storage.Reference ref = firebase_storage.FirebaseStorage.instance
//       .ref()
//       .child('images')
//       .child('defaultProfile.png');

  // Future<void> listExample() async {
  //   firebase_storage.ListResult result =
  //       await firebase_storage.FirebaseStorage.instance.ref().listAll();

  //   result.items.forEach((firebase_storage.Reference ref) {
  //     print('Found file: $ref');
  //   });

  //   result.prefixes.forEach((firebase_storage.Reference ref) {
  //     print('Found directory: $ref');
  //   });
  // }

  // Future<void> uploadFile(String filePath) async {
  //   File file = File(filePath);
    
  //   await firebase_storage.FirebaseStorage.instance
  //       .ref('uploads/file-to-upload.png')
  //       .putFile(file);
  // }


  // Future<void> uploadString() async {
  //   String dataUrl = 'data:text/plain;base64,SGVsbG8sIFdvcmxkIQ==';
  //     await firebase_storage.FirebaseStorage.instance
  //         .ref('uploads/hello-world.text')
  //         .putString(dataUrl, format: firebase_storage.PutStringFormat.dataUrl);
    
  // }


  //

  final _addItemFormKey = GlobalKey<FormState>();

  bool _isProcessing = false;

  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _addItemFormKey,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(
              left: 8.0,
              right: 8.0,
              bottom: 24.0,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 24.0),
                Text(
                  'Title',
                  style: TextStyle(
                    color: CustomColors.firebaseGrey,
                    fontSize: 22.0,
                    letterSpacing: 1,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 8.0),
                CustomFormField(
                  isLabelEnabled: false,
                  controller: _titleController,
                  focusNode: widget.titleFocusNode,
                  keyboardType: TextInputType.text,
                  inputAction: TextInputAction.next,
                  validator: (value) => Validator.validateField(
                    value: value,
                  ),
                  label: 'Title',
                  hint: 'Enter your note title',
                ),
                SizedBox(height: 24.0),
                Text(
                  'Description',
                  style: TextStyle(
                    color: CustomColors.firebaseGrey,
                    fontSize: 22.0,
                    letterSpacing: 1,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 8.0),
                CustomFormField(
                  maxLines: 10,
                  isLabelEnabled: false,
                  controller: _descriptionController,
                  focusNode: widget.descriptionFocusNode,
                  keyboardType: TextInputType.text,
                  inputAction: TextInputAction.done,
                  validator: (value) => Validator.validateField(
                    value: value,
                  ),
                  label: 'Description',
                  hint: 'Enter your note description',
                ),
                SizedBox(height: 24.0),
                Text(
                  'upload Map',
                  style: TextStyle(
                    color: CustomColors.firebaseGrey,
                    fontSize: 22.0,
                    letterSpacing: 1,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ),
          _isProcessing
              ? Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(
                      CustomColors.firebaseOrange,
                    ),
                  ),
                )
              : Container(
                  width: double.maxFinite,
                  child: ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(
                        CustomColors.firebaseOrange,
                      ),
                      shape: MaterialStateProperty.all(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ),
                    onPressed: () async {
                      widget.titleFocusNode.unfocus();
                      widget.descriptionFocusNode.unfocus();

                      if (_addItemFormKey.currentState!.validate()) {
                        setState(() {
                          _isProcessing = true;
                        });


//calling data insert to DB
                        await Database.addItem(
                          date: DateTime.now(),
                          title: _titleController.text,
                          description: _descriptionController.text,
                        );

                        setState(() {
                          _isProcessing = false;
                        });

                        Navigator.of(context).pop();
                      }
                    },
                    child: Padding(
                      padding: EdgeInsets.only(top: 16.0, bottom: 16.0),
                      child: Text(
                        'Add Stall',
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.bold,
                          color: CustomColors.firebaseGrey,
                          letterSpacing: 2,
                        ),
                      ),
                    ),
                  ),
                ),
        ],
      ),
    );
  }
}
